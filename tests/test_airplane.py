from src.airplane import Airplane


# TEST 1 - Nowo utworzony samolot ma “przebieg” 0 i ilość zajętych siedzeń 0 #
def test_new_plane_has_zero_mileage_and_zero_occupied_seats():
    test_plane = Airplane("Boeing", 500)
    assert test_plane.total_distance_flown == 0
    assert test_plane.number_of_occupied_seats == 0


# TEST 2 - Samolot po przeleceniu 5000 km ma wylatane 5000 km #
def test_mileage_of_plane_5000_km():
    test_plane = Airplane("Airbus", 350)
    test_plane.fly(5000)
    assert test_plane.total_distance_flown == 5000


# TEST 3 - Samolot po przeleceniu 5000 km a następnie 3000 km ma wylatane 8000 km #
def test_mileage_of_plane_5000_km_and_3000_km():
    test_plane = Airplane("Airbus", 350)
    test_plane.fly(5000)
    test_plane.fly(3000)
    assert test_plane.total_distance_flown == 8000


# TEST 4 - Samolot po 9999 km nie wymaga serwisu #
def test_service_after_flying_9999_km():
    test_plane = Airplane("Boeing", 500)
    test_plane.fly(9999)
    assert test_plane.is_service_required() == False


# TEST 5 - Samolot po 10001 km nie wymaga serwisu #
def test_service_after_flying_10001_km():
    test_plane = Airplane("Boeing", 500)
    test_plane.fly(10001)
    assert test_plane.is_service_required() == True


# TEST 6 - Samolot o maksymalnej ilości siedzeń 200 po załadowaniu 180 pasażerów ma dostępne 20 miejsc #
def test_not_a_full_plane():
    test_plane = Airplane("Boeing", 200)
    test_plane.board_passengers(180)
    assert test_plane.get_available_seats() == 20


# TEST 7 - Samolot o maksymalnej ilości siedzeń 200 po załadowaniu 201 pasażerów ma dostępne 0 miejsc i zajętych 200 miejsc #
def test_full_plane():
    test_plane = Airplane("Boeing", 200)
    test_plane.board_passengers(201)
    assert test_plane.get_available_seats() == 0
    assert test_plane.number_of_occupied_seats == 200
