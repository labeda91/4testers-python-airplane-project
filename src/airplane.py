class Airplane:
    def __init__(self, name, number_of_seats):
        self.name = name
        self.number_of_seats = number_of_seats
        self.total_distance_flown = 0
        self.number_of_occupied_seats = 0

    def fly(self, distance):
        self.total_distance_flown += distance

    def is_service_required(self):
        if self.total_distance_flown > 10000:
            return True
        else:
            return False

    def board_passengers(self, number_of_passengers):
        if (self.number_of_occupied_seats + number_of_passengers) < self.number_of_seats:
            self.number_of_occupied_seats += number_of_passengers
        elif (self.number_of_occupied_seats + number_of_passengers) == self.number_of_seats:
            print(f"The maximum number of passengers in {self.name} has been reached.")
            self.number_of_occupied_seats += number_of_passengers
        else:
            print(f"The maximum number of passengers in {self.name} has been reached. The remaining"
                  f" {self.number_of_occupied_seats + number_of_passengers - self.number_of_seats} "
                  f"people will not fly.")
            self.number_of_occupied_seats = self.number_of_seats

    def get_available_seats(self):
        return self.number_of_seats - self.number_of_occupied_seats


if __name__ == '__main__':
    plane1 = Airplane("Boeing", 500)
    plane2 = Airplane("Airbus", 350)

    plane1.fly(3654)
    plane2.fly(10001)

    plane1.fly(6345)

    print(f"{plane1.name} flew {plane1.total_distance_flown} km. {plane1.name} is service required: {plane1.is_service_required()}")
    print(f"{plane2.name} flew {plane2.total_distance_flown} km. {plane2.name} is service required: {plane2.is_service_required()}")

    plane1.board_passengers(499)
    plane2.board_passengers(349)

    plane1.board_passengers(128)

    print(f"Number of places still available in {plane1.name} is {plane1.get_available_seats()}.")
    print(f"Number of places still available in {plane2.name} is {plane2.get_available_seats()}.")
